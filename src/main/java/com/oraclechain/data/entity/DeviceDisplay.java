package com.oraclechain.data.entity;

import java.util.Date;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/4/7
 */
public class DeviceDisplay {

    private Long deviceId;
    /**
     * 设备名称
     */
    private String name;
    /**
     * 设备型号
     */
    private String unitType;

    /**
     * 生产日期
     */
    private Date producedTime;

    /**
     * 保修期
     */
    private String warranty;

    /**
     * 设备类型
     */
    private String type;

    /**
     * 所在舱体
     */
    private Long capsule;
    /**
     * 所在舱体组
     */
    private String capsuleGroup;
    /**
     * 设备位置
     */
    private String capsuleLocation;

    /**
     * 设备协议
     */
    private String protocol;

    /**
     * 设备状态
     */
    private Integer status;

    /**
     * 设备参数
     */
    private String parameters;


    /**
     * 健康值
     */
    private String healthIndex;


    /**
     * 设备阈值
     */
    private String threshold;

    /**
     * 所属PLCname
     */
    private String PlcName;

    /**
     * 所属PLCname
     */
    private String overhaul;

    private String theory;

    private String vendorName;

    private String code;

    private Date createTime;

    private Date updateTime;


    private Object value;

    private Long gatherTime;

    private String position;

    private Long startTime;

    private Long endTime;

    public DeviceDisplay(){

    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public Date getProducedTime() {
        return producedTime;
    }

    public void setProducedTime(Date producedTime) {
        this.producedTime = producedTime;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getCapsule() {
        return capsule;
    }

    public void setCapsule(Long capsule) {
        this.capsule = capsule;
    }

    public String getCapsuleGroup() {
        return capsuleGroup;
    }

    public void setCapsuleGroup(String capsuleGroup) {
        this.capsuleGroup = capsuleGroup;
    }

    public String getCapsuleLocation() {
        return capsuleLocation;
    }

    public void setCapsuleLocation(String capsuleLocation) {
        this.capsuleLocation = capsuleLocation;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getHealthIndex() {
        return healthIndex;
    }

    public void setHealthIndex(String healthIndex) {
        this.healthIndex = healthIndex;
    }

    public String getThreshold() {
        return threshold;
    }

    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

    public String getPlcName() {
        return PlcName;
    }

    public void setPlcName(String plcName) {
        PlcName = plcName;
    }

    public String getOverhaul() {
        return overhaul;
    }

    public void setOverhaul(String overhaul) {
        this.overhaul = overhaul;
    }

    public String getTheory() {
        return theory;
    }

    public void setTheory(String theory) {
        this.theory = theory;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Long getGatherTime() {
        return gatherTime;
    }

    public void setGatherTime(Long gatherTime) {
        this.gatherTime = gatherTime;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public static DeviceDisplay toDisplay(Device device) {
        DeviceDisplay deviceDisplay = new DeviceDisplay();
        deviceDisplay.setDeviceId(device.getId());
        deviceDisplay.setName(device.getName());
        deviceDisplay.setUnitType(device.getUnitType());
        deviceDisplay.setProducedTime(device.getProducedTime());
        deviceDisplay.setWarranty(device.getWarranty());
        deviceDisplay.setType(device.getType());
        deviceDisplay.setCapsule(device.getCapsule());
        deviceDisplay.setCapsuleGroup(device.getCapsuleGroup());
        deviceDisplay.setCapsuleLocation(device.getCapsuleLocation());
        deviceDisplay.setProtocol(device.getProtocol());
        deviceDisplay.setStatus(device.getStatus());
        deviceDisplay.setParameters(device.getParameters());
        deviceDisplay.setHealthIndex(device.getHealthIndex());
        deviceDisplay.setThreshold(device.getThreshold());
        deviceDisplay.setPlcName(device.getPlcName());
        deviceDisplay.setOverhaul(device.getOverhaul());
        deviceDisplay.setTheory(device.getTheory());
        deviceDisplay.setVendorName(device.getVendorName());
        deviceDisplay.setCode(device.getCode());
        deviceDisplay.setValue(device.getValue());
        deviceDisplay.setGatherTime(device.getGatherTime());
        deviceDisplay.setPosition(device.getPosition());
        return deviceDisplay;
    }
}
