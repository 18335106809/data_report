package com.oraclechain.data.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/1
 */
public enum DeviceDataTypeEnum {
    AVG("avg"), MAX("max");

    private String type;

    DeviceDataTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
