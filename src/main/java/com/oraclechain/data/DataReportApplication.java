package com.oraclechain.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/30
 */
@SpringBootApplication
@EnableAutoConfiguration
public class DataReportApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataReportApplication.class);
    }
}
