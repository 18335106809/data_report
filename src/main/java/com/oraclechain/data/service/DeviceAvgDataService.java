package com.oraclechain.data.service;

import com.oraclechain.data.entity.DeviceDataReport;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/1
 */
public interface DeviceAvgDataService {
    void insertData(DeviceDataReport deviceAvgData);
}
