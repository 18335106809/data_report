package com.oraclechain.data.service;

import com.oraclechain.data.entity.Device;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/30
 */
public interface CommonService {
    List<Device> getDeviceList();
}
