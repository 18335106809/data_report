package com.oraclechain.data.service.impl;

import com.oraclechain.data.dao.DeviceAvgDataDao;
import com.oraclechain.data.entity.DeviceDataReport;
import com.oraclechain.data.service.DeviceAvgDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/1
 */
@Service
public class DeviceAvgDataServiceImpl implements DeviceAvgDataService {

    static Logger logger = LoggerFactory.getLogger(DeviceAvgDataServiceImpl.class);

    @Autowired
    private DeviceAvgDataDao deviceAvgDataDao;

    @Override
    public void insertData(DeviceDataReport deviceAvgData) {
        deviceAvgDataDao.insert(deviceAvgData);
    }
}
