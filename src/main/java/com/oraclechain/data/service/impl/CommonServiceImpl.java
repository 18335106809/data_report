package com.oraclechain.data.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.data.config.RedisKeyConfig;
import com.oraclechain.data.entity.Device;
import com.oraclechain.data.service.CommonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/30
 */
@Service
public class CommonServiceImpl implements CommonService {

    static Logger logger = LoggerFactory.getLogger(CommonServiceImpl.class);

    @Value("${daily.avg.device.type}")
    private String deviceType;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public List<Device> getDeviceList() {
        logger.info("从redis中获取设备列表");
        List<Device> result = new ArrayList<>();
        try {
            Set<String> keys = redisTemplate.keys(RedisKeyConfig.DEVICE_KEY + "*");
            for (String key : keys) {
                String data = redisTemplate.opsForValue().get(key);
                Device device = JSONObject.parseObject(data, Device.class);
                if (!StringUtils.isEmpty(device.getType()) && deviceType.contains(device.getType().trim())) {
                    result.add(device);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
