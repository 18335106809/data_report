package com.oraclechain.data.dao;

import com.oraclechain.data.entity.DeviceDataReport;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/1
 */
@Mapper
public interface DeviceAvgDataDao {
    void insert(DeviceDataReport deviceAvgData);
    List<DeviceDataReport> select(DeviceDataReport deviceAvgData);
}
