package com.oraclechain.data.schedule;

import com.oraclechain.data.entity.*;
import com.oraclechain.data.service.CommonService;
import com.oraclechain.data.service.DeviceAvgDataService;
import com.oraclechain.data.util.MongoTemplateUtil;
import com.oraclechain.data.util.StringToArrayUtil;
import com.oraclechain.data.util.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/30
 */
@Component
@EnableScheduling
public class DeviceSchedule {

    static Logger logger = LoggerFactory.getLogger(DeviceSchedule.class);

    @Autowired
    private CommonService commonService;

    @Autowired
    private MongoTemplateUtil mongoTemplateUtil;

    @Autowired
    private DeviceAvgDataService deviceAvgDataService;

    static DecimalFormat decimalFormat = new DecimalFormat("#####0.00");

    // 根据设备、类型、求每日平均值，每日凌晨一点求前一天的
//    @Scheduled(cron = "0 0 1 * * *")
    @Scheduled(cron = "0 0/10 * * * *")
    public void getDataAvgByDay() {
        logger.info("定时任务，计算设备的每日平均值");
        try {
            List<Device> deviceList = commonService.getDeviceList();
            Long startTime = TimeUtil.beforeDayStart();
            Long endTime = TimeUtil.beforeDayEnd();
            for (Device device : deviceList) {
                DeviceDisplay deviceDisplay = new DeviceDisplay();
                deviceDisplay.setDeviceId(device.getId());
                deviceDisplay.setStartTime(startTime);
                deviceDisplay.setEndTime(endTime);
                for (String dataKey : StringToArrayUtil.toArray(device.getParameters())) {
                    DeviceDataResult deviceDataResult = mongoTemplateUtil.getAvgAndMax(device.getId(), startTime, endTime, dataKey);
                    Double avgValue = deviceDataResult.getAvg();
                    Double maxValue = deviceDataResult.getMax();
                    if (avgValue != null) {
                        avgValue = Double.valueOf(decimalFormat.format(avgValue));
                    } else {
                        avgValue = 0.0;
                    }
                    if (maxValue != null) {
                        maxValue = Double.valueOf(decimalFormat.format(maxValue));
                    } else {
                        maxValue = 0.0;
                    }
                    DeviceDataReport deviceAvgData = new DeviceDataReport(device.getId(), device.getCapsule(), dataKey, avgValue, maxValue, TimeUtil.beforeDay());
                    deviceAvgDataService.insertData(deviceAvgData);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("getDataAvgByDay定时任务出错");
        }
    }
}
