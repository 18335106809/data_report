package com.oraclechain.data.util;

import sun.applet.Main;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.DoubleToIntFunction;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/6/30
 */
public class TimeUtil {

    static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    static SimpleDateFormat formatDay = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static Long beforeDayStart() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.DATE, -1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        currentTime = calendar.getTime();
        return currentTime.getTime();
    }

    public static Long beforeDayEnd() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        currentTime = calendar.getTime();
        return currentTime.getTime();
    }

    public static String beforeDay() {
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.DATE, -1);
        currentTime = calendar.getTime();
        return format.format(currentTime);
    }

    // 取当前时间后的时间集合
    public static List<String> getBetweenDate(String end) {
        List<String> betweenList = new ArrayList<>();
        try {
            Date currentTime = new Date();
            betweenList = getBetweenDate(format.format(currentTime), end);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return betweenList;
    }

    // 取两个时间点内的时间集合
    public static List<String> getBetweenDate(String begin, String end) {
        List<String> betweenList = new ArrayList<>();
        try{
            Calendar startDay = Calendar.getInstance();
            startDay.setTime(format.parse(begin));
            if (startDay.getTimeInMillis() > format.parse(end).getTime()) {
                return betweenList;
            }
            startDay.add(Calendar.DATE, -1);
            while(true){
                startDay.add(Calendar.DATE, 1);
                Date newDate = startDay.getTime();
                String newEnd=format.format(newDate);
                betweenList.add(newEnd);
                if(end.equals(newEnd)){
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return betweenList;
    }

    public static Long beforeDayStart(String time) throws Exception {
        Date currentTime = format.parse(time);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.DATE, -1);
        currentTime = calendar.getTime();
        System.out.println(formatDay.format(currentTime));
        return currentTime.getTime();
    }

    public static Long beforeDayEnd(String time) throws Exception {
        Date currentTime = format.parse(time);
        System.out.println(formatDay.format(currentTime));
        return currentTime.getTime();
    }

    public static String beforeDay(String time) throws Exception {
        Date currentTime = format.parse(time);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentTime);
        calendar.add(Calendar.DATE, -1);
        currentTime = calendar.getTime();
        return format.format(currentTime);
    }

    public static void main(String[] args) throws Exception {
        List<String> timeList = getBetweenDate("2020-06-28", "2020-06-30");
        for (String day : timeList) {
            System.out.println(beforeDay(day));
        }
    }

}
