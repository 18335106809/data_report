package com.oraclechain.data.util;

import com.oraclechain.data.entity.DeviceDataResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/1/8
 */
@Component
public class MongoTemplateUtil {

    static Logger logger = LoggerFactory.getLogger(MongoTemplateUtil.class);

    public static final String COLLECTION_DEVICE = "device";

    @Autowired
    private MongoTemplate mongoTemplate;

    public <T> List<T> findPageByVo(T data, Class<T> clazz, int pageNo, int pageSize) throws Exception {
        Sort sort = new Sort(Sort.Direction.DESC, "gatherTime");
        Query findQuery = Query.query(findQueryByVo(data)).with(sort);
        if (pageSize != -1) {
            if (pageNo < 1)
                pageNo = 1;
            if (pageSize < 1)
                pageSize = 10;
            findQuery = findQuery.skip((pageNo - 1) * pageSize).limit(pageSize);
        }
        logger.debug(">>>{}", findQuery);
        return mongoTemplate.find(findQuery, clazz);
    }

    public <T> List<T> findListByVo(Object data, Class<T> clazz, Sort sort, String collection) {
        if (sort == null) {
            sort = new Sort(Sort.Direction.DESC, "gatherTime");
        }
        Query findQuery = Query.query(findQueryByVo(data)).with(sort);
        logger.debug(">>>{}", findQuery);
        return mongoTemplate.find(findQuery, clazz, collection);
    }

    public <T> long findTotalByVo(T data, Class<T> clazz) {
        Query findQuery = Query.query(findQueryByVo(data));
        logger.debug(">>>{}", findQuery);
        return mongoTemplate.count(findQuery, clazz);
    }

    public DeviceDataResult getAvgAndMax(Long deviceId, Long startTime, Long endTime, String dataKey) {
        DeviceDataResult deviceDataResult = new DeviceDataResult();
        try {
            // 封装查询条件
            List<AggregationOperation> operations = new ArrayList<>();
            operations.add(Aggregation.match(Criteria.where("deviceId").is(deviceId)));
            operations.add(Aggregation.match(Criteria.where("gatherTime").gte(startTime)));
            operations.add(Aggregation.match(Criteria.where("gatherTime").lte(endTime)));
            operations.add(Aggregation.group("deviceId").avg("value." + dataKey).as("avg").max("value." + dataKey).as("max"));
            Aggregation aggregation = Aggregation.newAggregation(operations);
            AggregationResults<DeviceDataResult> results = mongoTemplate.aggregate(aggregation, COLLECTION_DEVICE, DeviceDataResult.class);
            deviceDataResult.setAvg(results.getUniqueMappedResult().getAvg());
            deviceDataResult.setMax(results.getUniqueMappedResult().getMax());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deviceDataResult;
    }

    public Criteria findQueryByVo(Object data) {
        Criteria query = new Criteria();
        try {
            Criteria atQuery = null;
            Class<?> clazz = data.getClass();
            Field[] fields = clazz.getDeclaredFields();
            Field field;
            for (int i = 0; i <  fields.length; i++) {
                field = fields[i];
                //打开私有访问
                field.setAccessible(true);
                if( Modifier.isStatic(field.getModifiers())) {
                    continue;
                }
                //获取属性
                String name = field.getName();
                //获取属性值
                Object value = field.get(data);
                if (value != null) {
                    if (name.equals("startTime")) {
                        query.and("gatherTime").gte(value);
                    } else if (name.equals("endTime")) {
                        query.and("gatherTime").lte(value);
                    } else {
                        query.and(name).is(value);
                    }
                }
            }
            return query;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }

    public <T> void save(T data, String collectionName) {
        mongoTemplate.save(data, collectionName);
    }

}
